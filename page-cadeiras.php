<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

	<main id="content" class="<?php echo odin_classes_page_full(); ?>" tabindex="-1" role="main">

			<?php odin_breadcrumbs(); ?>

	
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

					
				endwhile;
			?>
			
			<?php  

			$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

			$args = array(
			    // 'category_name' => 'video',
			    'post_type' => 'cadeira',
			    'posts_per_page' => 3,
			    'paged' => $paged
			);

			$the_query = new WP_Query( $args );


			?>

			<?php if ( $the_query->have_posts() ) : ?>

				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<h2><?php the_title(); ?></h2>
					
					<?php echo odin_thumbnail( 200, 300, 'Meu texto alternativo', true, 'minha-classe' ); ?>

					<div class="entry-content">

					<?php the_content(); ?> 
					</div>

					<a href="<?php the_permalink(); ?>">Ver Detalhes</a>
					<hr>
					
				<?php endwhile;  ?>

			
				<?php

					
					echo odin_pagination( 2, 1, false, $the_query );

					// odin_paging_nav();

					// if (function_exists(custom_pagination)) {
						// custom_pagination($the_query->max_num_pages,"",$paged);
					// }
			    ?>

				<?php wp_reset_postdata(); ?>


			<?php else:  ?>

				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

		<style type="text/css">
				
			.minha-classe {
				float: right;
			}
			
			#breadcrumbs {
			    list-style:none;
			    margin:10px 0;
			    overflow:hidden;
			}
			  
			#breadcrumbs li {
			    display:inline-block;
			    vertical-align:middle;
			    margin-right:15px;
			}
			  
			#breadcrumbs .separator {
			    font-size:18px;
			    font-weight:100;
			    color:#ccc;
			}

			#breadcrumbs ul {
				padding-left: 0px !important;
			}

		</style>
	</main><!-- #main -->

<?php
get_footer();
