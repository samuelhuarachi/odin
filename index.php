<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

	<main id="content" class="<?php echo odin_classes_page_sidebar(); ?>" tabindex="-1" role="main">

			<?php  

			$opcoes = get_option('odin_general');
			$opcoes2 = get_option('odin_adsense');

			

			$bar = get_user_option( 'show_admin_bar_front', get_current_user_id() );
			

			$args = array(
			    // 'category_name' => 'video',
			    'post_type' => 'video'
			);

			$the_query = new WP_Query( $args );


			?>

			<?php if ( $the_query->have_posts() ) : ?>

				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<h2><?php the_title(); ?></h2>
					
					<?php echo odin_thumbnail( 200, 300, 'Meu texto alternativo', true, 'minha-classe' ); ?>

					<div class="entry-content">

					<?php the_content(); ?> 
					</div>

					<a href="<?php the_permalink(); ?>">Ver Detalhes</a>


					<?php wp_reset_postdata(); ?>
				<?php endwhile;  ?>

			<?php else:  ?>

				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

			<!-- wp_reset_postdata(); -->

			<style>
				.minha-classe {
					float: right;
				}
			</style>

			<h1 style="color:<?php echo $opcoes["cor_do_tema"]; ?>;">Cor do Tema</h1>

			<?php
				if ( have_posts() ) :
					// Start the Loop.
					while ( have_posts() ) : the_post();

						/*
						 * Include the post format-specific template for the content. If you want to
						 * use this in a child theme, then include a file called called content-___.php
						 * (where ___ is the post format) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );

					endwhile;

					// Post navigation.
					odin_paging_nav();

				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>

	</main><!-- #content -->

<?php
get_sidebar();
get_footer();
