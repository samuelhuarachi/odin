<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

		<main id="content" class="<?php echo odin_classes_page_sidebar(); ?>" tabindex="-1" role="main">

			
			<?php odin_breadcrumbs(); ?>

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();


					?>
					
					<?php echo odin_thumbnail( 200, 300, 'Meu texto alternativo', true, 'minha-classe' ); ?>

					<?php
					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );

					?>
					
					<?php

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				endwhile;
			?>

			<style>
				.minha-classe {
					float: right;
				}

				#breadcrumbs {
				    list-style:none;
				    margin:10px 0;
				    overflow:hidden;
				}
				  
				#breadcrumbs li {
				    display:inline-block;
				    vertical-align:middle;
				    margin-right:15px;
				}
				  
				#breadcrumbs .separator {
				    font-size:18px;
				    font-weight:100;
				    color:#ccc;
				}
			</style>
		</main><!-- #main -->

<?php


get_footer();
